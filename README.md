GBARPGMaker
===========

A program to make GBA programming easier, especially targeted on RPG games.

It parses all graphics for the game and generates a C library to use which handles graphics, collision and other more high level features.
Documentation is available [here](https://gbarpgmaker.readthedocs.io) but for now it is only in the Czech language.
