=======
Použití
=======

Používání se dělí na dvě části, používání samotného programu pro genezi knihovny a pak používání knihovny.
V části :ref:`pythonp` popisuji strukturu a fungování samotného programu a v části :ref:`cp` jsou popsané jednotlivé funkce a proměnné vygenerované knihovny.

.. toctree::
    :maxdepth: 2
    :glob:

    usage-python.rst
    usage-c.rst
