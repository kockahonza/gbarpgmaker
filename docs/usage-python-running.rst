.. _running:

Spouštění programu
==================

Po nainstalování lze program spustit příkazem ``python -m GBARPGMaker``.
Program předpokládá jeden argument, což je příkaz.
Program umí 3 různé příkazy.
Pokud program po spuštění (jakéhokoliv příkazu) nenajde ve složce, kde byl spuštěn :ref:`konfigurační soubor <config>`, tak hodí chybovou hlášku.
Knihovna a všechny ostatní soubory se generují do složky ``source``, kterou program (nebudeli už existovat) vytvoří ve složce, kde byl spuštěn.

příkaz make
-----------
Toto je hlavní mód, který generuje knihovnu do složky ``source``.

příkaz genmain
--------------
Toto je jednoduchý příkaz který do složky ``source`` vygeneruje jakousi předlohu pro main.c soubor.

příkaz inter
------------
Tento mód není pro normální používání, ale pro debuggování, když něco nefunguje.
Inter je zkratka pro interactive mode.
Program vytvoří instanci hlavní třídy, ale ještě ji ani nezpracuje ani nevypíše knihovnu.
Poté uživateli spustí `ipython <https://ipython.org>`_ konzoli.
Je tedy nutné, aby měl uživatel nainstalovaný `ipython <https://ipython.org>`_.
