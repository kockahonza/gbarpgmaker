.. _pythonp:

Generování knihovny podle konfiguračního souboru
================================================

.. toctree::
    :maxdepth: 2
    :glob:

    usage-python-running.rst
    usage-python-config.rst
    usage-python-maps.rst
    usage-python-sprite_graphics.rst
