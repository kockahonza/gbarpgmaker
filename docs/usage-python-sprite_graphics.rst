.. _sprity:

Sprite graphics
===============
Sprity jsou grafické objekty, které se můžou jednoduše pohybovat po obrazovce GBA.
Jsou většinou využívány na věci jako postavičky nebo například kapky deště.
To co nazývám sprite graphics je "vzhled" nebo několik "vzhledů".
Sprity, které používají stejný sprite graphics, budou pravděpodobně sprity stejného typu (například dvě kapky deště, vypadají stejně ale jsou na různých místech).

Interně mohou mít sprity jenom určité kombinace šířky a výšky (vždy jsou to násobky 8).
GBARPGMaker ale zpracuje obrázek jakýchkoliv rozměrů s tím, že výška i šířka musí být menší jak 64 a celý násobek 8.

Pokud není specifikován parametr :ref:`size` pak je celý obrázek považován za jeden snímek, pokud by některý rozměr přesáhl 64, tak se obrázek v tom rozměru ořízne.

GBARPGMaker ale umí z jednoho obrázku vygenerovat více snímků jednoho spritu.
To se hodí například, když uživatel chce vytvořit animovanou postavičku.
Bude editovat jeden png soubor a také bude v C kódu používat funkci ``setSpriteGraphicsFrame`` s tím, že jí zadá kolikátý snímek se má momentálně použít.
Pro použití více snímkových sprite graphics je třeba splnit několik podmínek.

- V obrázku musí být jednotlivé snímky vedle sebe - ne pod sebou
- V konfigurační souboru je třeba zvolit parametr :ref:`size` pro daný sprite graphics
- Šířka i výška obrázku musí přesně odpovídat. Obrázek musí jít beze zbytku rozdělit na snímky o zvolené velikost.

Když jsou podmínky splněny, pak se vše už udělá samo.

Pro každý obrázek platí, že nesmí mít víc jak 255 různých barev.
GBA má totiž pro sprity vyhrazeno pouze 256 barev.
Je to tedy velmi hrubý filtr, který odhalí obrázky, které by nikdy nešlo celé nahrát do paměti.
