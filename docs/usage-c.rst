.. _cp:

Použití vygenerované knihovny
=============================
Knihovna se skládá z několika souborů.
Soubory ``imageData.c`` a ``imageData.h`` obsahují vygenerovaná data, vesměs jsou to obrázky, palety, mapy ve všelijakých polích a k tomu jejich délky.
Také obsahují ``pravé velikosti`` jednotlivých :ref:`sprity`.

Soubory ``graphics.c`` a ``graphics.h`` obsahují všechny funkce, které se očekává, že bude volat uživatel.
Jsou to inicializovací funkce, funkce, kterými uživatel přiřadí spritu nějaký sprite graphics, kterými změní mapu, nebo kterými posouvá hráče a aktualizuje displej.

.. toctree::
    :maxdepth: 2
    :glob:

    usage-c-graphics.rst
    usage-c-imageData.rst
