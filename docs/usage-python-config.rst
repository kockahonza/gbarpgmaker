.. _config:

Konfigurační soubor
===================
Konfigurační soubor je python soubor, který obsahuje určité proměnné.
Nachází se ve složce, kde byl program spuštěn a jmenovat se musí ``config.py``, pokud to tak nebude, program nedoběhne.
Jsou to dvě python "dictionaries" ``maps`` a ``sprite_graphics`` a pak dva python listy ``targets`` a ``excluded_targets``.
Konfigurační soubor by mohl vypadat například takto:

.. code:: python

    maps = {
        "map1": {
            "tmx_path": "/path/to/map.tmx",
            "bottom_layer_name": "Floor",
            "middle_layer_name": "Walls",
            "top_layer_name": "Sky",
            "special_layer_name": "Specials",
        },
    }
    sprite_graphics = {
        "HeroAnimated": {
            "image_path": "/path/to/img.png",
            "size": [1, 1],
        },
        "SingleFrameExplosion": {
            "image_path": "/path/to/img.png",
        },
    }

    targets = ["main.c", "imageData.c", "graphics.c"]
    excluded_targets = ["main.c"]

Listy ``targets`` a ``excluded_targets``
----------------------------------------
Začnu popisem listů ``targets`` a ``excluded_targets``, protože jsou jednodušší.
Knihovna se skládá z několika souborů ( ``main.c``, ``imageData.c``, ``imageData.h``, ``graphics.c``, ``graphics.h`` ).
Pokud v konfiguračním souboru nebude ani jeden z listů, pak se vygenerují všechny s tím, že přepíšou staré soubory.
Proto se dá buď pomocí listu ``excluded_targets`` specifikovat, které soubory se nemají vygenerovat nebo pomocí ``targets``, které se mají vygenerovat (lze použít oba dva listy současně).

Slovník ``maps``
----------------
Zde jsou specifikovány jednotlivé mapy.
Klíčem slovníku je vždy jméno mapy (podle něj se pak budou jmenovat funkce v knihovně) a hodnotou je další slovník.
Zde se pro každou mapu specifikuje několik parametrů.

``image_path``
^^^^^^^^^^^^^^
Jeho hodnota je textový řetězec obsahující umístění obrázku.
Program by měl fungovat se všemi populárními formáty obrázků, ale ozkoušen je jenom s png, proto doporučuji použít ten.

``bottom_layer_name``, ``middle_layer_name`` a ``top_layer_name``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Jsou pro každou mapu povinné.
Jsou to textové řetězce, každý z nich určuje název vrstvy (layeru) v tmx souboru, která má být použita na danou funkci.
Detailnější popis je v části :ref:`Vzhled mapy <vzhled>`.

.. _specialln:

``special_layer_name``
^^^^^^^^^^^^^^^^^^^^^^
Není povinný, ale je potřeba pro použití :ref:`specialac`.
Je to opět textový řetězec, který určuje název vrstvy v tmx souboru, která popisuje :ref:`specialac`.

Slovník ``sprite_graphics``
---------------------------
Zde jsou určeny obrázky, které poté lze přiřadit spritům.
Pro podrobnější popis spritů si přečtěte část :ref:`sprity`.
Klíče ve slovníku ``sprite_graphics`` jsou jména daných sprite obrázků a hodnoty jsou další slovníky.
V nich se dají sprite graphics specifikovat dva parametry.

``image_path``
^^^^^^^^^^^^^^
Jeho hodnota je textový řetězec jehož obsah je umístění obrázku.
Program by měl fungovat se všemi populárními formáty obrázků, ale ozkoušen je jenom s png, proto doporučuji použít ten.

.. _size:

``size``
^^^^^^^^
Ten není nutné specifikovat.
To je list dvou celých čísel.
První číslo je šířka a druhé je výška jednoho snímku daného sprite graphics s tím, že číslo je počet dlaždic (počet pixelů děleno 8).
Tato hodnota je potřeba pokud chcete z jednoho souboru vygenerovat více snímků (opět, více rozebráno v části :ref:`sprity`).

