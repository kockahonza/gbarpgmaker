import sys
import os

sys.path.insert(0, os.path.abspath('../../'))


project = 'GBARPGMaker'
copyright = '2019, Jan Kočka'
author = 'Jan Kočka'
version = ''
release = '1.0'

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.imgmath",
    "sphinx.ext.todo",
    "breathe"
]

breathe_projects = { "testprj": "./doxygen/xml" }
breathe_default_project = "testprj"

breathe_domain_by_extension = {
        "h" : "c",
        "c" : "c",
        }

templates_path = ['_templates']
source_suffix = '.rst'
master_doc = 'index'
language = 'cs'
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']
pygments_style = 'sphinx'


html_theme = 'alabaster'
html_theme_options = {
    'page_width': '60%',
    'sidebar_width': '280px',
}
html_static_path = ['_static']
html_sidebars = {
    "index": ['about.html', 'globaltoc.html', 'relations.html', 'searchbox.html'],
    "**": ['about.html', 'globaltoc.html', 'relations.html', 'searchbox.html'],
}
html_title = "GBARPGMaker"
htmlhelp_basename = 'GBARPGMakerdoc'



# LaTeX ----------------------------------------------------------------

latex_documents = [
    (master_doc, 'GBARPGMaker.tex', 'GBARPGMaker Documentation',
     'Jan Kočka', 'manual'),
]

latex_use_modindex = False
latex_elements = {
    "papersize": "a4paper",
    "pointsize": "12pt",
    "fontpkg": r"\usepackage{mathpazo}",
}
latex_use_parts = True
latex_additional_files = ["flaskstyle.sty"]
latex_show_urls = 'footnote'


man_pages = [
    (master_doc, 'gbarpgmaker', 'GBARPGMaker Documentation',
     [author], 1)
]


texinfo_documents = [
    (master_doc, 'GBARPGMaker', 'GBARPGMaker Documentation',
     author, 'GBARPGMaker', 'One line description of project.',
     'Miscellaneous'),
]


epub_title = project
epub_exclude_files = ['search.html']
