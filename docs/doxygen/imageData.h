/*! \file */
#ifndef IMAGE_DATA_H
#define IMAGE_DATA_H

/**
 * Velikost v bytech pole \link __map_name__Palette \endlink .
 */
#define __map_name__PaletteLen {{ 2 * map.palette|length }}
/**
 * Pole, které obsahuje paletu k dané mapě v takovém formátu aby se dalo rychle kopírovat do paměti.
 */
extern const unsigned short __map_name__Palette[{{ map.palette|length }}];

/**
 * Velikost v bytech pole \link __map_name__Tileset \endlink .
 */
#define __map_name__TilesetLen {{ 4 * map.tileset|length }}
/**
 * Pole, které obsahuje tileset (dlaždice) k dané mapě v takovém formátu aby se dalo rychle kopírovat do paměti.
 */
extern const unsigned int __map_name__Tileset[{{ map.tileset|length }}];

/**
 * Rozměry dané mapy v jednotlivých políčkách (v tilech).
 * První je šířka, druhý je výška.
 */
extern const int __map_name__Size[2];

/**
 * Pole, které obsahuje číslo pro každé jednotlivé políčko na mapě.
 * Toto číslo určuje zdali je toto políčko místo nějaké speciální akce.
 * První bit zleva určuje jeli políčko zeď.
 * Druhé zleva jestli je to speciální akce prvního typu, třetí třetího typu a tak dále.
 */
extern const int __map_name__Specials[{{ map.specials|length }}];

/**
 * Velikost v bytech pole \link __map_layer_name__ \endlink .
 */
#define __map_layer_name__Len {{ 2 * map.layer_tile_maps[i]|length }}
/**
 * Rozměry dané vrstvy dané mapy v jednotlivých políčkách (v tilech).
 * První je šířka, druhý je výška.
 *
 * Zatím je vždy stejné jako \link __map_layer_name__Size \endlink, tato proměnná je připravená pro další funkce.
 */
extern const int __map_layer_name__Size[2];
/**
 * Pole, které obsahuje mapu dlaždic dané vrstvy dané mapy v takovém formátu aby se dalo rychle kopírovat do paměti.
 */
extern const unsigned short __map_layer_name__[{{ map.layer_tile_maps[i]|length }}];

/**
 * Interní proměnná, kterou uživatel nemusí používat.
 * Je v ní uložená hodnota, která je potřeba načíst do jednoho registru, aby sprite měl správný tvar.
 * Spolu s \link __sprite_graphic_name__Size \endlink tvoří kombinaci.
 */
#define __sprite_graphic_name__Shape {{ sprite_graphic.shape_size[0] }}
/**
 * Interní proměnná, kterou uživatel nemusí používat.
 * Je v ní uložená hodnota, která je potřeba načíst do jednoho registru, aby sprite měl správný tvar.
 * Spolu s \link __sprite_graphic_name__Shape \endlink tvoří kombinaci.
 */
#define __sprite_graphic_name__Size {{ sprite_graphic.shape_size[1] }}
/**
 * Pravá velikost spritu.
 * Ta, která je buď specifikovaná v konfiguračním souboru a nebo je automaticky odhadnuta podle velikost obrázku.
 * Tato hodnota je využívaná například k počítání kolizí.
 */
extern const unsigned short __sprite_graphic_name__RealSize[2];

/**
 * Velikost v bytech pole \link __sprite_graphic_name__Palette \endlink .
 */
#define __sprite_graphic_name__PaletteLen {{ sprite_graphic.palette|length }}
/**
 * Pole, které obsahuje paletu k danému sprite graphics v takovém formátu aby se dalo rychle kopírovat do paměti.
 */
extern const unsigned short __sprite_graphic_name__Palette[{{ sprite_graphic.palette|length }}];

/**
 * Velikost v bytech pole \link __sprite_graphic_name__Tileset \endlink .
 */
#define __sprite_graphic_name__TilesetLen {{ sprite_graphic.tileset|length }}
/**
 * Velikost v bytech jednoho snímku daného sprite graphics.
 */
#define __sprite_graphic_name__FrameLen {{ 2 * (sprite_graphic.gba_size[0] * sprite_graphic.gba_size[1])|int }}
/**
 * Pole, které obsahuje tileset (dlaždice) k danému sprite graphics v takovém formátu aby se dalo rychle kopírovat do paměti.
 */
extern const int __sprite_graphic_name__Tileset[{{ sprite_graphic.tileset|length }}];

/**
 * Tileset, který určuje vzhled bubliny na text.
 * Je fixně daná a je v 4bpp formátu aby zabíral co nejméně místa.
 */
const unsigned int BorderTextTileset[24];
/**
 * Paleta, která určuje barvy bubliny na text.
 * Je fixně daná.
 */
const unsigned short BorderTextPal[4];
/**
 * Základní font, který je využíván textovým systémem.
 * Pochází z tohoto projektu https://github.com/dhepper/font8x8.
 */
char default_font[128][8];
#endif
