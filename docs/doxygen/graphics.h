/*! \file */
#ifndef GRAPHICS_H
#define GRAPHICS_H

/**
 * Po zavolání přesune polohu kamery tak aby v levém horním rohu byly vidět specifikované souřadnice mapy.
 * Funkce se snaží být chytrá a měnit co nejméně paměti, pokud není nová pozice moc daleko od té první, pak pouze změní offset jednotlivých pozadí.
 */
void moveScreen(int screenx, int screeny);

/**
 * Stejné jako moveScreen(), ale nesnaží se být chytrá, vždy aktualizuje vše.
 */
void forceMoveScreen(int screenx, int screeny);

/**
 * @brief Změní aktuální mapu a spolu s ní i callbacky pro speciální akce.
 *
 * Místo \_\_map_name\_\_ je třeba dát jméno mapy zvolené v konfiguračním souboru.
 * Vygeneruje se jedna takováto funkce pro každou mapu.
 *
 * Po změně mapy zavolá funkci setSpecialActions(), které předá oba argumenty, tím se změní callbacky.
 */
void loadMap__map_name__(void (*heroSpecialActions_[])(int x, int y), int heroSpecialActionCount_);

/**
 * Atributy všech 128 spritů, do OAM se zkopírují až při volání funkcí updateSpriteAttr0(), updateSpriteAttr1(), updateSpriteAttr2().
 */
u16 spriteAttrs[128][3];
/**
 * Opravdové velikosti spritů, ty co uživatel může specifikovat v konfigurační souboru.
 * Používají se při výpočtech kolizí a podobných operací.
 */
int spriteRealSizes[128][2];

/**
 * \_\_sprite_graphics_name\_\_ je třeba nahradit jménem ``sprite graphics``, které je určeno v konfigurační souboru.
 *
 * Načte data dané ``sprite_graphics`` do paměti vyhrazené spritům.
 */
void loadSpriteGraphic__sprite_graphics_name__();

/**
 * \_\_sprite_graphics_name\_\_ je třeba nahradit jménem ``sprite graphics``, které je určeno v konfigurační souboru.
 *
 * Pokud byl daný ``sprite graphics`` načtený jako poslední (a proto je na konci paměti) pak se z paměti odebere.
 * Pokud ne, tak funkce nic neudělá.
 */
void unloadSpriteGraphic__sprite_graphics_name__();

/**
 * Vynuluje veškerou pamět pro grafiku spritů a tím umožní načtení jiných.
 */
void clearSpriteGraphics();

/**
 * Uloží nultý atribut určeného spritu do OAM paměti.
 *
 * \param spriteIndex určuje sprite (0 až 127).
 */
void updateSpriteAttr0(int spriteIndex);
/**
 * Uloží první atribut určeného spritu do OAM paměti.
 *
 * \param spriteIndex určuje sprite (0 až 127).
 */
void updateSpriteAttr1(int spriteIndex);
/**
 * Uloží druhý atribut určeného spritu do OAM paměti.
 *
 * \param spriteIndex určuje sprite (0 až 127).
 */
void updateSpriteAttr2(int spriteIndex);

/**
 * Uloží určenému spritu x-ovou souřadnici do \link spriteAttrs \endlink a pak zavolá updateSpriteAttr0(), čímž uloží hodnotu rovnou i do OAM.
 * X-ová souřadice je v tomto případě přímo x-ová souřadnice spritu, určuje tedy přímo polohu na obrazovce, ne na mapě, pokud chcete sprite umístit na mapu podívejte se na funkci placeSpriteOnMap().
 *
 * \param spriteIndex určuje sprite (0 až 127).
 * \param x je x-ová souřadnice.
 */
void setSpriteX(int spriteIndex, int x);

/**
 * Uloží určenému spritu y-ovou souřadnici do \link spriteAttrs \endlink a pak zavolá updateSpriteAttr1(), čímž uloží hodnotu rovnou i do OAM.
 * Y-ová souřadice je v tomto případě přímo y-ová souřadnice spritu, určuje tedy přímo polohu na obrazovce, ne na mapě, pokud chcete sprite umístit na mapu podívejte se na funkci placeSpriteOnMap().
 *
 * \param spriteIndex určuje sprite (0 až 127).
 * \param y je y-ová souřadnice.
 */
void setSpriteY(int spriteIndex, int y);

/**
 * Vrátí x-ovou hodnotu, kterou má sprite momentálně uloženou v \link spriteAttrs \endlink.
 *
 * \param spriteIndex určuje sprite (0 až 127).
 */
int getSpriteX(int spriteIndex);

/**
 * Vrátí y-ovou hodnotu, kterou má sprite momentálně uloženou v \link spriteAttrs \endlink.
 *
 * \param spriteIndex určuje sprite (0 až 127).
 */
int getSpriteY(int spriteIndex);

/**
 * Nastaví v \link spriteAttrs \endlink viditelnost spritu na 0 a pak zavolá updateSpriteAttr0(), čímž uloží hodnotu rovnou i do OAM.
 *
 * \param spriteIndex určuje sprite (0 až 127).
 */
void setSpriteDisable(int spriteIndex);

/**
 * Nastaví v \link spriteAttrs \endlink viditelnost spritu na 1 a pak zavolá updateSpriteAttr0(), čímž uloží hodnotu rovnou i do OAM.
 *
 * \param spriteIndex určuje sprite (0 až 127).
 */
void setSpriteEnable(int spriteIndex);

/**
 * Nastaví v \link spriteAttrs \endlink prioritu podle argumentu p a pak zavolá updateSpriteAttr2(), čímž uloží hodnotu rovnou i do OAM.
 *
 * \param p určuje požadovanou prioritu.
 * \param spriteIndex určuje sprite (0 až 127).
 */
void setSpritePriority(int spriteIndex, int p);

/**
 * Nastaví interní proměnnou tak, že sprite bude momentálně vykreslován na mapu (při zavolání funkce updateMap()) podle pozice, která mu byla posledně nastavená funkcí placeSpriteOnMap().
 *
 * \param spriteIndex určuje sprite (0 až 127).
 */
void showSpriteOnMap(int spriteIndex);

/**
 * Nastaví interní proměnnou tak, že sprite nebude vykreslován na mapu.
 *
 * \param spriteIndex určuje sprite (0 až 127).
 */
void hideSpriteOnMap(int spriteIndex);

/**
 * Nastaví interní proměnné, které určují polohu spritu, pokud je vykreslován na mapu.
 * Zároveň zavolá showSpriteOnMap().
 *
 * \param spriteIndex určuje sprite (0 až 127).
 * \param x určuje x-ovou souřadnici na mapě, jednotkou jsou jednotlivé pixely.
 * \param y určuje y-ovou souřadnici na mapě, jednotkou jsou jednotlivé pixely.
 */
void placeSpriteOnMap(int spriteIndex, int x, int y);

/**
 * \_\_sprite_graphics_name\_\_ je třeba nahradit jménem ``sprite graphics``, které je určeno v konfigurační souboru.
 * Nastaví \link spriteAttrs \endlink spritu tak, aby jeho vzhled odpovídal vzhledu zvoleného snímku daného ``sprite graphics`` a poté zavolá updateSpriteAttr0(), updateSpriteAttr1() a updateSpriteAttr2().
 * Nastaví také \link spriteRealSizes \endlink tak, aby velikost seděla na RealSize zvoleného ``sprite graphics``.
 * 
 * \param spriteIndex určuje sprite (0 až 127).
 * \param frameIndex určuje index snímku.
 */
void setSpriteGraphicsFrame__sprite_graphics_name__(int spriteIndex, int frameIndex);
/**
 * Zavolá setSpriteGraphicsFrame__sprite_graphics_name__(spriteIndex, 0).
 * 
 * \param spriteIndex určuje sprite (0 až 127).
 */
void setSpriteGraphics__sprite_graphics_name__(int spriteIndex);

/**
 * Struct pomocí kterého se předávají informace o nějaké speciální akci.
 * Obsahuje kód, který určuje typ speciální akce a polohu na které se special stal.
 *
 * \param code je číslo/kód/označení daného typu speciální akce.
 * \param position je pole integerů, které udávají polohu políčka speciální akce, která je popisována.
 */
struct special {
    int code;
    int position[2];
};
typedef struct special special;

/**
 * Struct na předávání více \link special \endlink .
 *
 * \param count je počet \link special \endlink které v poli jsou.
 * \param specials je samotné pole \link special \endlink .
 */
struct specialList {
    int count;
    special specials[64];
};
typedef struct specialList specialList;

/**
 * Tato funkce vrátí \link specialList \endlink všech \link special \endlink se kterými se argumenty určený sprite alespoň nějak překrývá.
 * 
 * \param spriteIndex určuje sprite (0 až 127).
 * \param spritex je x-ová souřadnice spritu na mapě v jednotlivých pixelech.
 * \param spritey je y-ová souřadnice spritu na mapě v jednotlivých pixelech.
 */
specialList spriteCheckForSpecials(int spriteIndex, int spritex, int spritey);

/**
 * Tato funkce postupně prochází všechny aktivní speciální akce a když se sprite s nějakým překrývá, tak to vrátí \link special \endlink který danou speciální akcí pokrývá.
 * 
 * \param spriteIndex určuje sprite (0 až 127).
 * \param spritex je x-ová souřadnice spritu na mapě v jednotlivých pixelech.
 * \param spritey je y-ová souřadnice spritu na mapě v jednotlivých pixelech.
 */
bool spriteCheckForAnySpecial(int spriteIndex, int spritex, int spritey);

/**
 * Vrátí true když vybraný sprite nějak zasahuje přes nějaké políčko zdi.
 * 
 * \param spriteIndex určuje sprite (0 až 127).
 * \param spritex je x-ová souřadnice spritu na mapě v jednotlivých pixelech.
 * \param spritey je y-ová souřadnice spritu na mapě v jednotlivých pixelech.
 */
bool spriteCheckForWalls(int spriteIndex, int spritex, int spritey);

/**
 * Vrátí true když se vybrané dva sprity překrývají na obrazovce.
 * 
 * \param spriteIndex1 určuje první ze zvolených spritů (0 až 127).
 * \param spritex1 je x-ová souřadnice prvního spritu na obrazovce v jednotlivých pixelech.
 * \param spritey1 je y-ová souřadnice prvního spritu na obrazovce v jednotlivých pixelech.
 * \param spriteIndex2 určuje druhý ze zvolených spritů (0 až 127).
 * \param spritex2 je x-ová souřadnice druhého spritu na obrazovce v jednotlivých pixelech.
 * \param spritey2 je y-ová souřadnice druhého spritu na obrazovce v jednotlivých pixelech.
 */
bool checkSpriteCollision(int spriteIndex1, int sprite1x, int sprite1y, int spriteIndex2, int sprite2x, int sprite2y);

/**
 * Vrátí true když se vybrané dva sprity překrývají na mapě (neusí na obrazovce vůbec být vidět.).
 * Další výhoda oproti checkSpriteCollision() je, že knihovna už ví polohu.
 * Pokud je alespoň jeden ze spritů na obrazovce momentálně skrytý (nebylo na něj zavoláno spriteShowOnMap()) tak funkce vrátí false.
 * 
 * \param spriteIndex1 určuje první ze zvolených spritů (0 až 127).
 * \param spriteIndex2 určuje druhý ze zvolených spritů (0 až 127).
 */
bool checkSpriteCollisionOnMap(int spriteIndex1, int spriteIndex2);

/**
 * @brief Změní callbacky pro speciální akce.
 *
 * Oba argumenty se starají o změnu callbacků.
 * První argument je pole pointerů na funkce, které berou 2 argumenty typu integer.
 * Funkce, na které ukazují pointery v poli jsou funkce, které se zavolají pokud hrdina vkročí na označené pole, nazývám je speciálními akcemi.
 * První speciální akce se zavolá pokud hrdina vkročí na ten typ speciálního políčka, které bylo ve zvolené mapě použité jako první.
 * Když se zavolá speciální akce, pak jsou jí předány dva argumenty, je to pozice 
 *
 * Druhý argument už je jednoduše počet speciálních akcí, které chcete aktivovat (počet položek v poli, které předáváte prvním argumentem).
 */
void setSpecialActions(void (*special_Actions_[])(int x, int y), int specialActionCount_);

/**
 * Aktualizuje mapu, stará se o posuv kamery, vykreslení hlavního hrdiny a spritů, které jsou zobrazeny na mapě a jsou v daný moment kamerou vidět.
 */
void updateMap();

/**
 * Inicializuje hlavní postavu.
 * \param hero_x_pointer_ pointer na integer, který v sobě má uloženou x-ovou souřadnici hlavního hrdiny.
 * \param hero_y_pointer_ pointer na integer, který v sobě má uloženou y-ovou souřadnici hlavního hrdiny.
 * \param hero_sprite_index_ je číslo spritu (0-127), které se bude využívat pro hlavního hrdinu.
 */
void initHero(int* hero_x_pointer_, int* hero_y_pointer_, int hero_sprite_index_);

/**
 * Vypíše předaný text.
 * V dolní polovině obrazovky vyskočí bublina, kde se text postupně objevuje.
 * Vypíše se vždy 130 znaků (víc se do bubliny najednou nevejde) a když uživatel zmáčkne tlačítko A, tak se zobrazí další část.
 *
 * \param text textový řetězec obsahující zprávu
 */
void printText(char* text);

/**
 * Inicializuje "textový systém".
 * Lze zvolit font, ale je třeba použít správný formát.
 * Pozor, inicializace textu zabere kus paměti pro pozadí, proto je možné že se do ní pak vejde méně různých dlaždic.
 * Text se vypisuje na 4 vrstvu pozadí.
 *
 * \param font 2D pole, font, který se má použít. Formát stejný jako zde: https://github.com/dhepper/font8x8. Pokud bude NULL tak se použije ten.
 */
void initText(char font[128][8]);

/**
 * Inicializuje pozadí a sprity.
 * Je nutné jí zavolat dříve než cokoliv kromě incializací.
 *
 * Nastaví priority vrstev, nastaví, které se mají zobrazit, vynuluje OAM \link spriteAttrs \endlink a nastaví interrupty a nastaví typ barev pozadí a spritů.
 * Všechny sprity mají prioritu 1, `bottom_layer` a `middle_layer` mají prioritu 3 a `top_layer` má prioritu 0.
 */
void initMap();

#endif
