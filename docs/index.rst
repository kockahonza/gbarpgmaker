Vítejte na stránkách dokumentace programu GBARPGMaker
=====================================================

GBARPGMaker je program, který má usnadnit práci programátorovi, který vytváří hru pro platformu Game Boy Advanced.
Program se nejvíce hodí pro vývoj her, kde je nějaká hlavní postavička, kterou sledujeme seshora a chodíme s ní po mapě.

Program funguje tím způsobem, že uživatel mu pomocí konfiguračního souboru určí mapy a obrázky spritů, které uživatel chce ve hře použít, a program poté vygeneruje knihovnu v jazyce C, pomocí které pak uživatel může určené mapy a obrázky použít. Uživatel, tedy musí programovat v jazyce C.

Mapy se programu předávají ve formátu tmx, který používá editor `Tiled <https://www.mapeditor.org/>`_ a obrázky jsou png soubory.
V C kódu už jsou pak uživateli přístupné funkce, které se starají o nastavování a vykreslování mapy, spritů a do značné míry i textu.

Uživatel pak také musí kód kompilovat.
Vygenerovaná knihovna využívá knihovnu `libgba <https://github.com/devkitPro/libgba>`_, kompilátor k ní tedy musí mít přístup.
Kompilovat kód na GBA lze několika způsoby, doporučuji projekt `devkitPro <https://devkitpro.org/index.php>`_ a jejich toolchain devkitARM (instrukce pro instalaci jsou zde `<https://devkitpro.org/wiki/Getting_Started>`_).
Pro testování již zkompilovaného programu je také velmi vhodné mít na počítači nainstalovaný emulátor.
Těch je mnoho, ale jeden z nejpřesnějších je `mGBA <https://mgba.io/>`_, který já osobně používám.
Program také obsahuje font z této stránky `<https://github.com/dhepper/font8x8>`_.
Nakonec pro informace o programování na GBA a návody také velmi silně doporučuji stránku `Tonc <http://coranac.com/tonc>`_, já jsem ji při psaní programu používal.

Samotný program je psaný v jazyce `Python <https://python.org>`_  a využívá několik základních modulů:

- argparse
- xml.dom.minidom

a dvě knihovny:

- `jinja2 <http://jinja.pocoo.org/>`_
- `wand <http://docs.wand-py.org/en/0.5.2/>`_

Veškerý kód programu lze najít na mém `gitlabu <https://gitlab.com/kockahonza/gbarpgmaker>`_.

Nakonec, dokumentace je generována pomocí programů `Sphinx <http://www.sphinx-doc.org/en/master/>`_ a `Doxygen <http://doxygen.nl/>`_ a hostovaná je službou `Read the docs <https://readthedocs.org/>`_.

.. only:: latex

    Dokumentace se stejným obsahem ale příjemnějším vzhledem než je v tomto pdf dokumentu je `zde <https://gbarpgmaker.readthedocs.io>`_.
    Pokud budete číst dokumentaci vygenerované knihovny, pak doporučuji jít tam, je to výrazně přehlednější.

.. only:: html

    Příručka pro uživatele
    ^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
    :maxdepth: 4
    :glob:

    installation.rst
    usage.rst
    examples.rst
    authors.rst
