======
Ukázky
======

Na `gitlabu <https://gitlab.com/kockahonza/gbarpgmaker>`_ jsou ve složce examples dvě ukázky, které obě obsahují mapy (tmx soubory), tilesety (tsx soubory), konfigurační soubor, kód samotné hry (soubor main.c) a Makefile (pro kompilaci s  `devkitPro <https://devkitpro.org/index.php>`_).

První je asi nejjednodušší možný základ, který ale má nedostatky (aby byl opravdu snadno pochopitelný).
Skládá se z míčku co se pohybuje na mapě, kde jsou stoly na které naráží a květiny na které když vkročí, tak dostane varování.

Druhým už je plně funkční hra, kde hráč běhá po budově a bombami zabíjí slimy, kteří zase naopak zraňují jeho.
Všechny soubory se dají stáhnout `zde <https://gitlab.com/kockahonza/gbarpgmaker>`_.
Ovládání je následující:

- šipky pro chození do stran
- L pro položení bomby (lze jich najednou položit 5)
- SELECT pro zobrazení skóre
- START pro zobrazení nejvyššího kdy dosaženého cíle (je možné, že se v některých emulátorech resetuje při každém spuštění)

V těchto dvou projektech se dá vygenerovat knihovna a zkompilovat kód pomocí následující příkazů (pokud máte nainstalovaný GBARPGMaker a `devkitPro <https://devkitpro.org/index.php>`_).

- Nejprve si spusťte terminál ve složce ukázky.
- Pak spustě příkaz ``python -m GBARPGMaker make``, tím se vygeneruje knihovna.
- Nakonec příkaz ``make``, tím se celý kód zkompiluje a vytvoří se soubor .gba, který už lze spustit na emulátoru.
