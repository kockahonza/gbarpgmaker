.. _mapy:

Mapy
====
Jedna mapa je v programu jedna lokace, statický "obrázek" po kterém chodí hráč a další objekty.
Mapy jsou generovány z tmx souborů, jedna mapa odpovídá vždy právě jednomu.


.. _vzhled:

Vzhled
------
Co se týče vzhledu, jedna mapa se skládá ze 3 vrstev.
Jsou jimi ``bottom_layer``, ``middle_layer`` a ``top_layer``.
Při kreslení se nejprve kreslí ``bottom_layer``, pak ``middle_layer`` a nakonec ``top_layer``.
Při editování v programu `Tiled <https://www.mapeditor.org/>`_ je tedy nutné vytvořit tři různé ``Tile Layery`` a v :ref:`Konfiguračním souboru <config>` určit správná jména.

Navíc je ``middle_layer`` také používán ke generování takzvaných ``Walls`` neboli zdí.
Každé políčko z vrstvy ``middle_layer``, které není prázdné, bude považováno za zeď a bude tedy pro hráče (případně další objekty) neprůchozí.

Při tvoření map v editoru je možné využít jakýkoliv počet tilesetů, které ale musí být 8x8 pixelů a také musí být založené na tileset obrázku (Based on Tileset Image, jak je to možné vidět v editoru).
Pokud nastane tato situace, program nehodí error.
Všechny dlaždice (tiles) ze všech tilesetů, které mapa používá se pak najednou načtou do paměti při načtení mapy.
Kvůli omezení GBA lze najednou mít v paměti pouze 256 různých barev na pozadí a 768 různých dlaždic (tiles).
Pokud mapa toto omezení překročí, program hodí error s chybovou hláškou.

Pokud chcete, aby nějaký tileset, který je v mapě použit, nebyl použit programem, lze to udělat pomocí volitelným parametrem (custom property) ``DontParse`` typu bool nastavenou na true.
Takový tileset pak nebude vůbec zpracován programem a políčka, na kterých byl daný tileset použit budou prázdné (černé).


.. _specialac:

Speciální akce
--------------
Na úrovni map je implementovaná funkce takzvaných :ref:`specialac`.
Je to v zásadě způsob odlišení jednotlivých políček jinak než jen vzhledem.

K využití této funkce je třeba vytvořit v editoru  `Tiled <https://www.mapeditor.org/>`_ další layer, který je nazýván ``special_layer``.
Jeho jméno je také třeba určit v :ref:`Konfiguračním souboru <config>` pomocí parametru :ref:`specialln`.

Je také výhodné vytvořit ještě jeden tileset, který bude používán výhradně touto vrstvou.
Tento tileset je pak třeba v editoru `Tiled <https://www.mapeditor.org/>`_ označit volitelným parametrem (custom property) ``SpecialLayer`` typu bool a nastavit ho na true.
Tento tileset se pak nebude parsovat a načítat do paměti, když by tento parametr nebyl přítomen, speciální akce budou stále fungovat, ale bude se plýtvat pamět.

Ve vrstvě ``special_layer`` pak můžete označovat políčka na nichž se má něco stát (například se hráči má zobrazit textová zpráva).
Pokud použijete stejnou dlaždici ze stejného tilesetu v jedné mapě víc jak jednou, tak tyto políčka budou mít stejnou funkci (budou mít stejný callback).
Ve vygenerované knihovně jsou pak typy akcí číslovány.
První :ref:`speciální akce <specialac>` pak bude odpovídat té dlaždici, která byla použita v mapě jako první (Je nejvýše a nejvíce vlevo).

Callbacky pro :ref:`specialac` jsou pak v knihovně nastaveny pomocí funkce **loadMap__map_name__** v souboru :ref:`graphics`.
Dají se změnit i bez změny mapy pomocí funkce **setHeroSpecialActions**.


