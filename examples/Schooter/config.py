maps = {
    "map1": {
        "tmx_path": "resources/FirstFloor.tmx",
        "bottom_layer_name": "Floor",
        "middle_layer_name": "Walls",
        "top_layer_name": "Sky",
        "special_layer_name": "Specials",
    },
    "map2": {
        "tmx_path": "resources/SecondFloor.tmx",
        "bottom_layer_name": "Floor2",
        "middle_layer_name": "Walls2",
        "top_layer_name": "Sky2",
        "special_layer_name": "Specials2",
    },
}
sprite_graphics = {
    "Shooter": {
        "image_path": "resources/sprites/hero.png",
        "size": [1, 1],
    },
    "Slime": {
        "image_path": "resources/sprites/slime.png",
        "size": [1, 1],
    },
    "Bomb": {
        "image_path": "resources/sprites/bomb.png",
        "size": [1, 1],
    },
    "Explosion": {
        "image_path": "resources/sprites/explosion.png",
    },
    "Life": {
        "image_path": "resources/sprites/life.png",
    },
}
