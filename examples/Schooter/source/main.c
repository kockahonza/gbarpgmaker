#include <gba.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "imageData.h"
#include "graphics.h"

#define MAXBOMBCOUNT 5
#define MAXSLIMECOUNT 30
#define MAXHEARTCOUNT 10
#define MAXMEDKITCOUNT 50

#define DEBUG true

/*
sprite/OAM layout:
    0 - hero
    1 to MAXBOMBCOUNT - bombs
    MAXBOMBCOUNT + 1 to MAXBOMBCOUNT + MAXBOMBCOUNT - explosions
    MAXBOMBCOUNT + MAXBOMBCOUNT + 1 to MAXBOMBCOUNT + MAXBOMBCOUNT + MAXSLIMECOUNT - slimes

    0 - hero
    1 to MAXSLIMECOUNT - slimes
    MAXSLIMECOUNT + 1 to MAXSLIMECOUNT + MAXBOMBCOUNT - bombs
    MAXSLIMECOUNT + MAXBOMBCOUNT + 1 to MAXSLIMECOUNT + MAXBOMBCOUNT + MAXBOMBCOUNT - explosions
    
    127 - MAXHEARTCOUNT + 1 to 127 - life hearts
*/

void DEBUGPrintText(char* text) {
    if (DEBUG) {
        printText(text);
    }
}

// setup hero vars
const int hero_sprite_index = 0;
int hero_animation_frame = 0;
const int hero_animation_interval = 60 / 3;
int hero_x = 80;
int hero_y = 80;
int hero_speed = 1;
const int hero_max_health = 200;
int hero_health = hero_max_health;

// setup bomb and explosion vars
typedef struct {
    int position[2];
    bool planted;
    int timer;
} bomb;

typedef struct {
    int position[2];
    int timer;
} explosion;

int next_bomb = 0;
const int bomb_stage_count = 3;
const int bomb_stage_delay = 60;
const int explosion_time = 30;
const int explosion_radius = 12;
const int explosion_damage = 60;

bomb bombs[MAXBOMBCOUNT];
explosion explosions[MAXBOMBCOUNT];

// setup slime vars
typedef struct {
    float position[2];
    int health;
} slime;

const int slime_max_health = 50;
const int slime_damage = 20;
int slime_count = 0;
unsigned int slime_kills = 0;
int slime_spawn_rate = 60;
int slime_spawn_radius = 10 * 8;
const int slime_decay_time = 60 * 3;
float slime_speed = 0.5;
slime slimes[MAXSLIMECOUNT];

void spawnSlime(int x, int y) {
    for (int i = 0; i < MAXSLIMECOUNT; ++i) {
        if (slimes[i].health == -1 * slime_decay_time) {
            slimes[i].health = slime_max_health;
            slimes[i].position[0] = (float) x;
            slimes[i].position[1] = (float) y;
            setSpriteGraphicsSlime(1 + i);
            placeSpriteOnMap(1 + i, x, y);
            slime_count += 1;
            return;
        }
    }
}

void killAllSlimes() {
    for (int i = 0; i < MAXSLIMECOUNT; ++i) {
        slimes[i].health = -1 * slime_decay_time;
        hideSpriteOnMap(1 + i);
    }
}

// HUD vars
int heart_spacing = 2;

// counter variable - loops values from 0 to 600, changes each run - 60 times a second
int counter = 0;

void moveHero(int kh, int counter) {
    if (keysHeld() & KEY_RIGHT) {
        for (int i = 0; i < hero_speed; i++) {
            hero_x += 1;
            if (counter % hero_animation_interval == 0) {
                hero_animation_frame = (hero_animation_frame + 1) % 2;
            }
            setSpriteGraphicsFrameShooter(hero_sprite_index, 4 + hero_animation_frame);
        }
    } else if (keysHeld() & KEY_LEFT) {
        for (int i = 0; i < hero_speed; i++) {
            hero_x -= 1;
            if (counter % hero_animation_interval == 0) {
                hero_animation_frame = (hero_animation_frame + 1) % 2;
            }
            setSpriteGraphicsFrameShooter(hero_sprite_index, 10 + hero_animation_frame);
        }
    } else if (keysHeld() & KEY_DOWN) {
        for (int i = 0; i < hero_speed; i++) {
            hero_y += 1;
            if (counter % hero_animation_interval == 0) {
                hero_animation_frame = (hero_animation_frame + 1) % 2;
            }
            setSpriteGraphicsFrameShooter(hero_sprite_index, 1 + hero_animation_frame);
        }
    } else if (keysHeld() & KEY_UP) {
        for (int i = 0; i < hero_speed; i++) {
            hero_y -= 1;
            if (counter % hero_animation_interval == 0) {
                hero_animation_frame = (hero_animation_frame + 1) % 2;
            }
            setSpriteGraphicsFrameShooter(hero_sprite_index, 7 + hero_animation_frame);
        }
    }
}

void (*specialActions[])(int x, int y);
int specialActionCount;

void setHighscore(int highscore) {
    *((u8*)0x0E000000) = (u8) (highscore);
    *((u8*)0x0E000001) = (u8) (highscore >> 8);
    *((u8*)0x0E000002) = (u8) (highscore >> 16);
    *((u8*)0x0E000003) = (u8) (highscore >> 24);
}

unsigned int getHighscore() {
    unsigned int p1 = ((unsigned int) *((u8*)0x0E000000));
    unsigned int p2 = ((unsigned int) *((u8*)0x0E000001)) << 8;
    unsigned int p3 = ((unsigned int) *((u8*)0x0E000002)) << 16;
    unsigned int p4 = ((unsigned int) *((u8*)0x0E000003)) << 24;
    return p1 + p2 + p3 + p4;
}

void deadHero() {
    char deadHeroMsg[100];
    if (slime_kills > getHighscore()) {
        setHighscore(slime_kills);
        sprintf(deadHeroMsg, "Good job, new highscore: %u", getHighscore());
    } else {
        sprintf(deadHeroMsg, "Your score is: %u. The current highscore is: %u.", slime_kills, getHighscore());
    }
    printText(deadHeroMsg);
    hero_health = hero_max_health;
    slime_kills = 0;
    killAllSlimes();
    for (int i = 0; i < MAXBOMBCOUNT; ++i) {
        bombs[i].planted = false;
        explosions[i].timer = 0;
        hideSpriteOnMap(1 + MAXSLIMECOUNT + i);
        hideSpriteOnMap(1 + MAXSLIMECOUNT + MAXBOMBCOUNT + i);
    }
    hero_x = 80;
    hero_y = 80;
    loadMapmap1(specialActions, specialActionCount);
}

void updateHealthBar(int heart_count) {
    for (int i = heart_count; i < MAXHEARTCOUNT; ++i) {
        setSpriteDisable(127 - i);
    }
    for (int i = 0; i < heart_count; ++i) {
        setSpriteEnable(127 - i);
    }
}

void updateHUD() {
    int heart_count = (int) (0.5 + MAXHEARTCOUNT * (hero_health / ((float) hero_max_health)));
    if (heart_count > MAXHEARTCOUNT) {
        updateHealthBar(MAXHEARTCOUNT);
    } else {
        updateHealthBar(heart_count);
    }
}

double getDistance(int x1, int y1, int x2, int y2) {
    return sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2));
}

// item vars
typedef struct {
    int pos[2];
    bool used;
} medkit;

medkit medkits[MAXMEDKITCOUNT];
int knownmedkit_count = 0;
int medkit_heal_amount = 40;

// setup special callbacks
void apply_medkit(int x, int y) {
    int medkit_index = -1;
    for (int i = 0; i < knownmedkit_count; ++i) {
        if ((medkits[i].pos[0] == x) && (medkits[i].pos[1] == y)) {
            medkit_index = i;
        }
    }
    if (medkit_index == -1) {
        if (knownmedkit_count < MAXMEDKITCOUNT) {
            medkit_index = knownmedkit_count;
            knownmedkit_count += 1;
            medkits[medkit_index].pos[0] = x;
            medkits[medkit_index].pos[1] = y;
        } else {
            DEBUGPrintText("knownmedkit_count would exceed MAXMEDKITCOUNT - increase MAXMEDKITCOUNT to fix this error");
            return;
        }
    }

    if (medkits[medkit_index].used == false) {
        if (hero_health + medkit_heal_amount > hero_max_health) {
            hero_health = hero_max_health;
        } else {
            hero_health += medkit_heal_amount;
        }
        medkits[medkit_index].used = true;

        updateHUD();
    }
}

void stairs_up(int x, int y) {
    loadMapmap2(specialActions, specialActionCount);
    killAllSlimes();
}

void stairs_down(int x, int y) {
    loadMapmap1(specialActions, specialActionCount);
    killAllSlimes();
}

void (*specialActions[])(int x, int y) = {apply_medkit, stairs_up, stairs_down};
int specialActionCount = 3;

void vid_vsync() {
    while(REG_VCOUNT >= 160) {
    }
    while(REG_VCOUNT < 160) {
    }
}


int main() {
    // init stuff and load sprites
    initMap();
    initText(NULL);

    loadSpriteGraphicShooter();
    loadSpriteGraphicSlime();
    loadSpriteGraphicBomb();
    loadSpriteGraphicExplosion();
    loadSpriteGraphicLife();

    // setup hero
    initHero(&hero_x, &hero_y, hero_sprite_index);

    setSpriteGraphicsFrameShooter(hero_sprite_index, 0);
    setSpriteEnable(hero_sprite_index);

    // setup slimes
    for (int i = 0; i < MAXSLIMECOUNT; ++i) {
        slimes[i].health = -1 * slime_decay_time;
        setSpriteGraphicsSlime(1 + i);
    }

    // setup bombs
    for (int i = 0; i < MAXBOMBCOUNT; ++i) {
        bombs[i].planted = false;
        setSpriteGraphicsBomb(1 + MAXSLIMECOUNT + i);
        setSpriteGraphicsExplosion(1 + MAXSLIMECOUNT + MAXBOMBCOUNT + i);
    }

    // HUD
    for (int i = 0; i < MAXHEARTCOUNT; ++i) {
        setSpriteX(127 - i, (8 + heart_spacing) * i + heart_spacing);
        setSpriteY(127 - i, 0);
        setSpriteGraphicsLife(127 - i);
    }

    // load first map
    loadMapmap1(specialActions, specialActionCount);

    updateHUD();
    updateMap();

    // check if highscore needs to be set
    if (getHighscore() == 0xFFFFFFFF) {
        setHighscore(0);
    }

    while (true) {
        scanKeys();
        int kh = keysHeld();
        int kd = keysDown();
        // takes care of arrow keys
        moveHero(kh, counter);
        if (kd & KEY_B) {
            if (bombs[next_bomb].planted == false) {
                placeSpriteOnMap(1 + MAXSLIMECOUNT + next_bomb, hero_x, hero_y);

                bombs[next_bomb].position[0] = hero_x;
                bombs[next_bomb].position[1] = hero_y;
                bombs[next_bomb].planted = true;
                bombs[next_bomb].timer = bomb_stage_count * bomb_stage_delay;
                next_bomb = (next_bomb + 1) % MAXBOMBCOUNT;
            }
        }
        if (kd & KEY_START) {
            char curHighscoreMsg[40];
            sprintf(curHighscoreMsg, "Current highscore is: %u", getHighscore());
            printText(curHighscoreMsg);
        }
        if (kd & KEY_SELECT) {
            char curScoreMsg[40];
            sprintf(curScoreMsg, "Current score is: %u", slime_kills);
            printText(curScoreMsg);
            /* for (int i = 0; i < MAXSLIMECOUNT; ++i) { */
            /*     if (slimes[i].health > 0) { */
            /*         slimes[i].health = 0; */
            /*         slime_count -= 1; */
            /*         slime_kills += 100; */
            /*         break; */
            /*     } */
            /* } */
        }

        int rnum = rand() % 3600;
        if (rnum <= slime_spawn_rate) {
            int newx;
            int newy;
            bool should_spawn = false;
            for (int i = 0; i < 100; ++i) {
                newx = hero_x + (rand() % (2 * slime_spawn_radius + 1)) - slime_spawn_radius;
                newy = hero_y + (rand() % (2 * slime_spawn_radius + 1)) - slime_spawn_radius;
                if (!spriteCheckForAnySpecial(1, newx, newy) && getDistance(hero_x, hero_y, newx, newy) > 5 * 8 + 4) {
                    should_spawn = true;
                    break;
                }
            }
            if (should_spawn) {
                spawnSlime(newx, newy);
            }
        }

        for (int i = 0; i < MAXSLIMECOUNT; ++i) {
            if (slimes[i].health > 0) {
                float dx = ((float) hero_x) - slimes[i].position[0];
                float dy = ((float) hero_y) - slimes[i].position[1];
                float xoffset = slime_speed * dx / fabs(dx);
                float yoffset = slime_speed * dy / fabs(dy);
                if((fabs(dx) < 6) && (fabs(dy) < 6)) {
                    hero_health -= slime_damage;
                    if (hero_health <= 0) {
                        deadHero();
                    }
                    updateHUD();
                    slimes[i].health = -1 * slime_decay_time;
                    hideSpriteOnMap(1 + i);
                    continue;
                }
                if ((dx * dx >= dy * dy) && !spriteCheckForAnySpecial(1 + i, slimes[i].position[0] + xoffset, slimes[i].position[1])) {
                    slimes[i].position[0] += xoffset;
                } else if (!spriteCheckForAnySpecial(1 + i, slimes[i].position[0], slimes[i].position[1] + yoffset)) {
                    slimes[i].position[1] += yoffset;
                } else if (!spriteCheckForAnySpecial(1 + i, slimes[i].position[0] + xoffset, slimes[i].position[1])) {
                    slimes[i].position[0] += xoffset;
                }
                if (counter % 60 == 0) {
                    setSpriteGraphicsFrameSlime(1 + i, rand() % 3);
                }
                placeSpriteOnMap(1 + i, (int) slimes[i].position[0], (int) slimes[i].position[1]);
            } else if (slimes[i].health > -1 * slime_decay_time + 1) {
                setSpriteGraphicsFrameSlime(1 + i, 3);
                slimes[i].health -= 1;
            } else if (slimes[i].health == -1 * slime_decay_time + 1){
                hideSpriteOnMap(1 + i);
                slimes[i].health -= 1;
            }
        }

        for (int i = 0; i < MAXBOMBCOUNT; ++i) {
            if (bombs[i].planted) {
                if (bombs[i].timer == 0) {
                    hideSpriteOnMap(1 + MAXSLIMECOUNT + i);
                    bombs[i].planted = false;
                    placeSpriteOnMap(1 + MAXSLIMECOUNT + MAXBOMBCOUNT + i, bombs[i].position[0] + BombRealSize[0] * 4 - ExplosionRealSize[0] * 4, bombs[i].position[1] + BombRealSize[1] * 4 - ExplosionRealSize[1] * 4);
                    explosions[i].timer = explosion_time;
                    if (getDistance(bombs[i].position[0] + BombRealSize[0] * 4, bombs[i].position[1] + BombRealSize[1] * 4, hero_x + ShooterRealSize[0] * 4, hero_y + ShooterRealSize[1] * 4) < explosion_radius) {
                        hero_health -= explosion_damage;
                        if (hero_health <= 0) {
                            deadHero();
                        }
                        updateHUD();
                    }
                    for (int o = 0; o < MAXSLIMECOUNT; ++o) {
                        if ((slimes[o].health > 0) && (checkSpriteCollisionOnMap(1 + o, 1 + MAXSLIMECOUNT + MAXBOMBCOUNT + i))) {
                            slimes[o].health -= explosion_damage;
                            if (slimes[o].health <= 0) {
                                slime_count -= 1;
                                slime_kills += 1;
                            }
                        }
                    }
                } else if (bombs[i].timer % bomb_stage_delay == 0) {
                    setSpriteGraphicsFrameBomb(1 + MAXSLIMECOUNT + i, bomb_stage_count - (bombs[i].timer / bomb_stage_delay));
                }
                bombs[i].timer -= 1;
            }
            if (explosions[i].timer > 0) {
                explosions[i].timer -= 1;
            } else {
                hideSpriteOnMap(1 + MAXSLIMECOUNT + MAXBOMBCOUNT + i);
            }
        }

        /* vid_vsync(); */
        VBlankIntrWait();
        updateMap();
        if (counter == 600) {
            counter = 0;
        } else {
            counter += 1;
        }
    }
}
