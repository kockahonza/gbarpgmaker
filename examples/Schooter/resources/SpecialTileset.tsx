<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="SpecialTileset" tilewidth="8" tileheight="8" tilecount="5" columns="5">
 <properties>
  <property name="SpecialTileset" type="bool" value="true"/>
 </properties>
 <image source="SpecialTileset.png" width="40" height="8"/>
</tileset>
