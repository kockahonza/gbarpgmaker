#include <gba.h>
#include <stdio.h>
#include <stdlib.h>

#include "imageData.h"
#include "graphics.h"

int hero_x = 10;
int hero_y = 10;

// warning for the ball, when it steps on a flower
void warning(int x, int y) {
    printText("Dont step on the flowers!");
}

int main() {
    // init stuff and load sprites
    initMap();
    initText(NULL);

    loadSpriteGraphicHero();

    // setup hero
    initHero(&hero_x, &hero_y, 0);

    setSpriteGraphicsHero(0);
    setSpriteEnable(0);

    // take care of special actions
    void (*specialActions[])(int x, int y) = {warning};

    // load the map
    loadMapmap1(specialActions, 1);
    updateMap();

    while (true) {
        scanKeys();
        if (keysHeld() & KEY_RIGHT) {
            hero_x += 1;
        } else if (keysHeld() & KEY_LEFT) {
            hero_x -= 1;
        } else if (keysHeld() & KEY_DOWN) {
            hero_y += 1;
        } else if (keysHeld() & KEY_UP) {
            hero_y -= 1;
        }

        // wait for the next VBlank before updating graphics
        VBlankIntrWait();
        updateMap();
    }
}
