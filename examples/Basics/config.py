maps = {
    "map1": {
        "tmx_path": "resources/Map.tmx",
        "bottom_layer_name": "Floor",
        "middle_layer_name": "Walls",
        "top_layer_name": "Sky",
        "special_layer_name": "Specials",
    },
}
sprite_graphics = {
    "Hero": {
        "image_path": "resources/sprites/hero.png",
        "size": [1, 1],
    },
}
